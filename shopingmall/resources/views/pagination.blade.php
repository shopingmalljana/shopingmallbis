<!--PAGINATION_START-->
<div class="row">
  <div class="col-md-3">
    
      <span class="glyphicon glyphicon-chevron-left" id="glyphicon_side"></span>
  </div>
  <div class="col-md-6">
    <ul class="pagination">

  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li class="disabled"><a href="#">4</a></li>
  <li><a href="#">5</a></li>
</ul>

  </div>
  <div class="col-md-3">
    
    <span class="glyphicon glyphicon-chevron-right" id="glyphicon_side"></span>
  </div>
</div>
<!--PAGINATION_END-->