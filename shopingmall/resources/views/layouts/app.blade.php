<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

    <!-- START font-awesome-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!--END font-awesome-->

  <!--START bootstrap-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--END bootstrap-->


    <link href="{{ asset('css/common.css') }}" rel="stylesheet">

    </head>
    <body>

        <!--START NAV BAR-->
   <nav class="navbar ">
  <div class="container-fluid" >
    <div class="navbar-header" id="navbar">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">LANKA Shopping MALL</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        
        <li><a href="#">HOME</a></li>
        <li><a href="{{ url('aboutus') }}">ABOUT US</a></li>
        
        <li><a href="#">SERVICE</a></li>
        <li><a href="#">PROJECT</a></li>
      </ul>
      @if (Route::has('login'))
      <ul class="nav navbar-nav navbar-right">
        
        
              
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <a href="{{ route('register') }}"><input type="button" value="Open your OWN shop" name="" id="btn_nav" class="btn btn-warning"></a>
                    @endauth
                
            @endif
        




         
      <li><a href="{{url('admin')}}"><input type="button" value="admin" name="" id="btn_admin" class="btn btn-danger"></a></li>
        

      </ul>
    </div>
  </div>

  <!--END OF NAV BAR-->
</nav>

   @include('advertisement_one')

    


    <!--START CONTAINER-->
  <div class="container">

    @yield('page_content')  
   
</div>

<!--END CONTAINER-->

@include('advertisement_tow')   
        
 <!--footer-->
<footer class="footer1">
<div class="container">

<div class="row"><!-- row -->
            
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                          <li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Useful links</h1>
                                
                                <ul>
                                  <li><a  href="#"><i class="fa fa-angle-double-right"></i> About Us</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i> Contact Us</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i> Success Stories</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i> PG Courses</a></li>
                                    
                                    
                                </ul>
                    
              </li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                          <li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Useful links</h1>
                                
                                <ul>
                  <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Test Series Schedule</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i>  Postal Coaching</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i>  PG Dr. Bhatia Books</a></li>
                                    <li><a  href="#"><i class="fa fa-angle-double-right"></i>  UG Courses</a></li>
                                    
                                   
                                </ul>
                    
              </li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column left -->
            
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                          <li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Useful links</h1>
                                
                                <ul>


                                  <li><a href="#"><i class="fa fa-angle-double-right"></i> Enquiry Form</a></li>
                                  <li><a href="#"><i class="fa fa-angle-double-right"></i> Online Test Series</a></li>
                                  <li><a href="#"><i class="fa fa-angle-double-right"></i> Grand Tests Series</a></li>
                                  <li><a href="#"><i class="fa fa-angle-double-right"></i> Subject Wise Test Series</a></li>
                                </ul>
                    
              </li>
                            
                        </ul>
                         
                      
                </div><!-- widgets column left end -->
                
                
                <div class="col-lg-3 col-md-3"><!-- widgets column center -->
                
                   
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                          <li class="widget-container widget_recent_news"><!-- widgets list -->
                    
                                <h1 class="title-widget">Contact Detail </h1>
                                
                                <div class="footerp"> 
                                
                                <h2 class="title-median">Lanka Shoping mall</h2>
                                <p><b>Email id:</b> <a href="#">lahiruj93@gmail.com.com</a></p>
                                
                               <p><b>Phone Numbers : </b>0719732545, </p>
                                 <p> 011-27568832, 9868387223</p>
                                </div>
                                
                                <div class="social-icons">
                                
                                  <ul class="nomargin">
                                    
                <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
              <a href="https://twitter.com"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
              <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
              <a href="#"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
                                    
                                    </ul>
                                </div>
                        </li>
                          </ul>
                       </div>
                </div>
</div>
</footer>
<!--header-->

<div class="footer-bottom">

  <div class="container">

    <div class="row">

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

        <div class="copyright">

          © 2017, Lanka Shoping Mall

        </div>

      </div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

        <div class="design">

           <a href="#">Janaranga & Navoda </a> |  <a target="_blank" href="#">Web Design & Development</a>

        </div>

      </div>

    </div>

  </div>
<!--END OF FOOTER-->
  
       
    </body>
</html>
