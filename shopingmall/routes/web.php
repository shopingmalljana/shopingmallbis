<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/profile', 'OwnerController@index');

/*nnnn*/
Route::get('/', 'HomeController@index')->name('home');

Route::get('aboutus','AboutusController@index')->name('aboutus');

Route::get('admin','AdminController@index');


